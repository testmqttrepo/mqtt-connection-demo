import { Injectable } from '@nestjs/common';
import {map} from  'rxjs';

import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from } from 'rxjs';
import { CIDERROR } from 'src/utils/common';
import { CIDException } from '../utils/http';
import { IUser, UserDto } from './models/user.dto';
import { User, UserDocument } from './models/user.schema';
import { checkIfFileOrDirectoryExists, getFile } from 'src/helper/storage.helper';

@Injectable()
export class UserService {
    constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) { }

    async createUser(createUserDto: UserDto): Promise<IUser> {
        const createdUser = new this.userModel(createUserDto);
        return await createdUser.save().catch(err => {
            console.log(err);
            throw new CIDException({cidErrors: [CIDERROR.USER_CREATION_FAILED]});
        });
    }

    async updateUser(updateUserDto: UserDto): Promise<IUser> {
        return await (await this.userModel.findByIdAndUpdate(updateUserDto._id, updateUserDto).catch(err => Promise.reject(CIDERROR.USER_UPDATE_FAILED))).save();
    }

    async deleteUser(_id: string): Promise<UserDocument> {
        return await this.userModel.findByIdAndRemove(_id);
    }

    async findAll(): Promise<IUser[]> {
        return this.userModel.find().exec();
    }

    async findOne(username: string): Promise<IUser> {
        return await this.userModel.findOne({firstName: username}).exec()
    }

    updateUserImageById(id,imagePath:string){
        const user:User = new User();
        user._id=id;
        user.imagePath=imagePath;
        return from(this.userModel.update(id,user))
        }

    findImageNameById(id){
        return from(this.userModel.findOne({id})).pipe(
            map((user:User)=>{
                return user.imagePath
            })
        )
    }

    async getExportedUserCSV(): Promise<string | Buffer> {
        console.log('fileName')
        const filePath = `D:/Aws learning/aws implementation in code/nestjs-poc-master/src/f518194738-private.pem`;
        console.log('filePath',filePath)
        if (!checkIfFileOrDirectoryExists(filePath)) {
        return null
        }
        const file=(await getFile(filePath))
        console.log('file',file)
        return file ;
    }

    }

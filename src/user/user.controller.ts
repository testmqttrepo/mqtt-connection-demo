

import { Body, Controller, Get, HttpCode, Post, Request, Res, UploadedFile,  UseFilters, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { saveImageToStorage } from 'src/helper/image-storage';
import { CIDExceptionFilter } from 'src/utils/filters/cid-exception.filter';
import { JwtAuthGuard } from 'src/utils/guards/jwt-auth.guard';
import { IUser, UserDto } from './models/user.dto';
import { UserService } from './user.service';
var express = require('express');
@Controller('user')
export class UserController {

    constructor(private userService: UserService) {

    }

    @Post('createUser')
    @HttpCode(200)
    @UseFilters(CIDExceptionFilter)
    async createUser(@Body() userDto: UserDto): Promise<IUser> {
        return await this.userService.createUser(userDto);
    }

    @Get()
    @UseGuards(JwtAuthGuard)
    @HttpCode(200)
    async getUsers(): Promise<IUser[]> {
        return await this.userService.findAll();
    }

    @Get('getCerts')
    @HttpCode(200)
    async getCerts(@Res() res){
        const fileName="f518194738-certificate.pem.crt"
        res.file
        return res.sendFile("D:/Aws learning/aws implementation in code/nestjs-poc-master/src/",fileName)
    }

    @Post('updateImage')
    @HttpCode(200)
    @UseInterceptors(FileInterceptor('file',saveImageToStorage))
    @UseFilters(CIDExceptionFilter)
    uploadImage(@UploadedFile() file:Express.Multer.File, @Request() req): Promise<any> {
        return ;
    }

@Get('/file')
async downloadImage(@Res() res ){ 
    console.log('in file download')
const fileName =await this.userService.getExportedUserCSV().then((csvData) => {
    return res.send(csvData);
})}
    
}

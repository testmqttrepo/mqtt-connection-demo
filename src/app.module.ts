import { Module, Logger } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { CommonService } from './services/common.service';
import { UserModule } from './user/user.module';
import { EMData } from './models/EMData.schema';
import { EMDataModule } from './EMData.module';
import { BullModule, InjectQueue } from '@nestjs/bull';
import { MessageConsumer } from './messege.consumer';
import { Queue } from 'bull';

var mongo = require('mongodb')
var mongc = mongo.MongoClient
var mqtt = require('mqtt')

const host = 'broker.hivemq.com'
const port = '1883'
const connectUrl = `mqtt://${host}:${port}`
var url ='mongodb://localhost/test'

var broker = mqtt.connect(connectUrl);    ///for hive community broker

// MongooseModule.forRoot('mongodb://localhost/test')
@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost/test'),
  BullModule.forRoot({ redis: {
    host: 'localhost',
    port: 6379,
  }}), 
  BullModule.registerQueue({
    name: 'firstQueueJob',
  }),
  AuthModule,EMDataModule],
  controllers: [AppController],
  providers: [AppService, CommonService,MessageConsumer],
  exports:[BullModule]
})
export class AppModule {
  constructor(@InjectQueue('firstQueueJob') private audioQueue: Queue, private appService:AppService ){
    this.mqttConnection()
  }

  


mqttConnection(){
  this.appService.mqttConnection()

  
//   let send=this
//   let msg

//   broker.on('connect', function () {
//     Logger.log('Connected to MQTT_Broker');
// });

// broker.on('error', function (error) {
//     console.log(error);
// });

// broker.on('message', function (topic, message) {
//   //Called each time a message is received
//   console.log('message received in mqtt')
// msg=JSON.parse(message)
//   send.appService.getHello()
//   //console.log('Received message:', topic, message.toString());
//   mongc.connect(url,(error,client)=>{
//     var myCol= client.db('test').collection('EM-1')
//     myCol.insertOne({
//       message:message.toString(),
//       createdAt: new Date()
//     },()=>{
//       console.log('data is inserted to mongodb')
//       client.close()
//     })
//   })
//   console.log('End of connection')
// });

// // subscribe to topic 'my/test/topic'
// broker.subscribe('/floatinitySystems/float@2021/ems/EM1');

// // publish message 'Hello' to topic 'my/test/topic'
// broker.publish('my/first/topic_1', 'Hello Form nestjs code');

}
}






  //setup the callbacks
 

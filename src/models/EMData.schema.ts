import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as S } from 'mongoose';
import { IEMData } from './EMData.dto';

export type EMDataDocument = EMData & Document;

@Schema({ timestamps: true})
export class EMData implements IEMData {
    _id: string;

}

export const EMDataSchema = SchemaFactory.createForClass(EMData);
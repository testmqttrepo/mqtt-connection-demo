import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
var awsIot = require('aws-iot-device-sdk');
const readline = require('readline');
import * as fs from 'fs';
var mqtt = require('mqtt')

import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { AppModule } from './app.module';
import { CommonService } from './services/common.service';
var awsIot = require('aws-iot-device-sdk');
var path = require('path');
var mongo = require('mongodb')
var mongc = mongo.MongoClient
var url ='mongodb://gauri:12345@localhost:27017/mqttJS'

async function bootstrap() {

  const logger = new Logger('Application');

  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({ logger: true })
  );

  app.enableCors();

  const common = app.get(CommonService);

  await app.listen(3000, () => {
    logger.log('Starting CID Application');
    logger.log('Current Server Time: ' + new Date());
    logger.log('Current MAC address of machine: ' + common.getMacAddress());

  });

  // * --    hiveMQ cnnection  Setting start --- * //
//   const host = 'broker.hivemq.com'
//   const port = '1883'
//   const connectUrl = `mqtt://${host}:${port}`
//   var options = {
//     host: '8525ec4da0ad4d6f896a8cf1097303c4.s1.eu.hivemq.cloud',
//     port: 8883,
//     protocol: 'mqtts',
//     username: 'gauri',
//     password: 'Floatinity@2021'
// }

// //initialize the MQTT client
// //var broker = mqtt.connect(options);     //for cloud cluster

// //
// var broker = mqtt.connect(connectUrl);    ///for hive community broker

// //setup the callbacks
// broker.on('connect', function () {
//     console.log('Connected');
// });

// broker.on('error', function (error) {
//     console.log(error);
// });

// broker.on('message', function (topic, message) {
//   //Called each time a message is received
//   console.log('Received message:', topic, message.toString());
 
//   mongc.connect(url,(error,client)=>{
//     var myCol= client.db('mqttJS').collection('mqttJS')
//     myCol.insertOne({
//       message:message.toString()
//     },()=>{
//       console.log('data is inserted to mongodb')
//       client.close()
//     })
//   })
// });

// // subscribe to topic 'my/test/topic'
// broker.subscribe('/flot/float@2021/testGW/EM');

// // publish message 'Hello' to topic 'my/test/topic'
// broker.publish('my/first/topic_1', 'Hello Form nestjs code');


// * --    hiveMQ cnnection  Setting End--- * //


// const readInterface = readline.createInterface({
//         input: fs.createReadStream('./src/f518194738-private.pem'),
//         output: process.stdout,
//         console: false
//     });

// for await (const line of readInterface) {
//         console.log(line);
//     }
// //or
// readInterface.on('line', function(line) {
//     console.log(line);
// });

// * --    aws cnnection  --- * //

//   var device = new awsIot.device({
//     keyPath:'./src/f518194738-private.pem',
//     certPath: './src/f518194738-certificate.pem.crt',
//     caPath: './src/x590.pem',
//     clientId: 'arn:aws:iot:ap-south-1:855898807067',
//     host: 'a1cidk49i3c84c-ats.iot.ap-south-1.amazonaws.com'
// });

// device
//     .on('connect', function() {
//         console.log('connect');
//         device.subscribe('topic_1');
//         device.publish('topic_2', JSON.stringify({ test_data_from_local_user: 1}));
//     });

//     device
//     .on('message', function(topic, payload) {
//         logger.log('message', topic, payload.toString());
//     });
}
bootstrap();
var awsIot = require('aws-iot-device-sdk');
//import { Logger } from '@nestjs/common';
//const logger = new Logger('Application');

var device = awsIot.device({
    keyPath: 'f518194738-private.pem',
    certPath: 'f518194738-certificate.pem.crt',
    caPath: 'x590.pem',
    clientId: 'arn:aws:iot:ap-south-1:855898807067',
    host: 'a1cidk49i3c84c-ats.iot.ap-south-1.amazonaws.com'
});

device
    .on('connect', function() {
        console.log('connect');
        device.subscribe('topic_1');
        device.publish('from_local_user', JSON.stringify({ test_data_local: 1}));
    });

    device
    .on('message', function(topic, payload) {
        console.log('message', topic, payload.toString());
    });
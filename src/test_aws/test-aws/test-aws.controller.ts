import { Controller } from '@nestjs/common';
var awsIot = require('aws-iot-device-sdk');

@Controller('test-aws')
export class TestAwsController {}
var device = awsIot.device({
    keyPath: 'D:\Aws learning\Policy crets\eceb47686b-private.pem.key',
    certPath: 'D:\Aws learning\Policy crets\eceb47686b-certificate.pem.crt',
    caPath: 'D:\Aws learning\Policy crets\x590.pem',
    clientId: 'arn:aws:iot:ap-south-1:855898807067',
    host: 'a1cidk49i3c84c-ats.iot.ap-south-1.amazonaws.com'
});

device
    .on('connect', function() {
        console.log('connect');
        device.subscribe('topic_1');
        device.publish('topic_2', JSON.stringify({ test_data: 1}));
    });

    device
    .on('message', function(topic, payload) {
        console.log('message', topic, payload.toString());
    });
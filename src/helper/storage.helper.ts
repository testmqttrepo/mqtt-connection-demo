import { promisify } from "util";
import * as fs from 'fs'

//  @param {string} path
//  @param {string} encoding

// @returns {Promise<Buffer>}

export const checkIfFileOrDirectoryExists = (path: string): boolean => {
    return fs.existsSync(path);
  };

export const getFile = async (
 path: string
): Promise<string | Buffer> => {
 const readFile = promisify(fs.readFile);

 return readFile(path, {});
};
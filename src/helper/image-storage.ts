import { diskStorage } from "multer";
import { v4 as uuidv4} from "uuid";


const fs = require('fs');
const FileType=require('file-type');
import path= require('path');


export const saveImageToStorage={
storage:diskStorage({
    destination:'/images',
    filename:(req,file,cb)=>{
        const fileExtension:string=path.extname(file.originalname);
        const fileName:string=file.originalname
        cb(null,fileName)
    }
    
})
}
import { InjectQueue, Process, Processor } from "@nestjs/bull";
import { Job, Queue } from "bull";


@Processor('firstQueueJob')
export class MessageConsumer {
    constructor(@InjectQueue('firstQueueJob') private audioQueue: Queue) {}

    @Process('message-job')
    async readOperationJob(job:Job<unknown>){
        console.log('start consumer')
        let value:any = job.data
        console.log('start consumer')
        this.audioQueue.add('message',{
            message:""
          })
        console.log('end consumer',value.message.time,value.message.testid)
    }

    @Process('message')
    async FirstPerform(job:Job<unknown>){
        console.log('performed First')
    }
}
import { Body, Controller, Get,Post, UseFilters } from '@nestjs/common';
import { AppService } from './app.service';
import { CIDERROR } from './utils/common';
import { CIDExceptionFilter } from './utils/filters/cid-exception.filter';
import { CIDException } from './utils/http';

@Controller('apppMod')
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('getHello')
  @UseFilters(CIDExceptionFilter)
  getHello(): string {
    //return 'Hello'
    //throw new CIDException({ apiStatus: 200, cidErrors: [CIDERROR.USER_EXISTS], data: null });
    return this.appService.getHello();

  }

  @Get('getMqtt')
  getMqtt(){
    const value=this.appService.mqttConnection()
return null
  }
}

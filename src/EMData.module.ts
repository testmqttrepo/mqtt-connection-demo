import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EMData, EMDataSchema } from './models/EMData.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: EMData.name, schema: EMDataSchema }])],
  controllers: [],
  providers: []
})
export class EMDataModule {}

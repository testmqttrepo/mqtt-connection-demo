import { Global, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EMData, EMDataDocument } from './models/EMData.schema';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
var mqtt = require('mqtt')
var mongo = require('mongodb')
var mongc = mongo.MongoClient
var mqtt = require('mqtt')
import { BullModule } from '@nestjs/bull';
import { QueueScheduler } from 'rxjs/internal/scheduler/QueueScheduler';
import { setTimeout } from 'timers/promises';



@Injectable()
export class AppService {
  value
constructor(@InjectQueue('firstQueueJob') private audioQueue: Queue) {
 
}

  getHello(): string {
    let message={
      time:1000,
    testid:'A'
    };
    this.audioQueue.add('message-job',{
      message
    })
    return 'Hello World!';
  }


    sendMessage(message){
    console.log('message recived in service')
    message={...message,
      time:1000,
    testid:'A'
    }
   // message.time=1000;
    this.audioQueue.add('message-job',{
      message
    })
    // this.audioQueue.add('message',{
    //   message
    // })
    console.log('end of the promise')
  }


async mqttConnection(){
  console.log('iin mqtt')
  var send=this
  let msg;
  const url ='mongodb://localhost/test'
  const host = 'broker.hivemq.com'
  const port = '1883'
  const connectUrl = `mqtt://${host}:${port}`
  const broker = mqtt.connect(connectUrl);
    broker.on('connect',function (){
      Logger.log('Connected to MQTT_Broker');
  });
 // msg='messege'

 // this.sendMessage(msg)
  
  broker.on('error', function (error) {
      console.log(error);
  });

   
  broker.on('message', function (topic, message) {
    //this.sendMessage(message)
    //Called each time a message is received
    console.log('////////////////////// msg /////////////////////////////')
    msg=JSON.parse(message)
    send.sendMessage(msg)
    //console.log('Received message:', topic, message.toString());
    mongc.connect(url,(error,client)=>{
      var myCol= client.db('test').collection('EM-1')
      myCol.insertOne({
        message:message.toString(),
        createdAt: new Date()
      },()=>{
       // console.log('data is inserted to mongodb')
        client.close()
      }
      )
    })
  });
  
  //console.log(msg);
  // subscribe to topic 'my/test/topic'
  broker.subscribe('/floatinitySystems/float@2021/ems/EM1');
  
  // publish message 'Hello' to topic 'my/test/topic'
  broker.publish('my/first/topic_1', 'Hello Form nestjs code');

}

}
